const stream = require("stream");
const { waitForEvent } = require("../00_promises/promises_lib");

const {
  SequenceStream,
  MultiplierStream,
  AsyncMultiplierStream,
  ArrayfierStream
} = require("./streams_lib");

(async () => {
  const before = process.hrtime();

  const pairs = [[0, 9], [10, 19], [20, 29]];

  const [first, second, third] = await Promise.all(
    pairs
      .map(pair => new SequenceStream(...pair))
      .map(stream => stream.pipe(new MultiplierStream(2)))
      // .map(stream => stream.pipe(new AsyncMultiplierStream(2)))
      .map(stream => stream.pipe(new ArrayfierStream()))
      .map(stream => waitForEvent(stream, "result"))
  );

  console.log("=> First", first);
  console.log("=> Second", second);
  console.log("=> Third", third);

  const delta = process.hrtime(before);

  console.log(`=> Completed in ${(delta[0] * 1e9 + delta[1]) / 1e6}ms`);
})();
