const stream = require("stream");
const { timeout } = require("../00_promises/promises_lib");

class SequenceStream extends stream.Readable {
  constructor(start, end) {
    super({
      objectMode: true,
      highWaterMark: 1
    });
    this.start = start;
    this.curr = start;
    this.end = end;
  }

  _read(count) {
    for (let remaining = count; remaining >= 1; remaining -= 1) {
      console.log("-- Pushing item", this.curr);
      this.push(this.curr);
      this.curr += 1;
      if (this.curr > this.end) {
        this.push(null);
        return;
      }
    }
  }
}

module.exports.SequenceStream = SequenceStream;

class MultiplierStream extends stream.Transform {
  constructor(factor) {
    super({
      objectMode: true
    });
    this.factor = factor;
  }

  _transform(item, enc, done) {
    console.log("-- Transforming item", item);
    this.push(item * this.factor);
    done();
  }
}

module.exports.MultiplierStream = MultiplierStream;

class AsyncMultiplierStream extends stream.Transform {
  constructor(factor) {
    super({ objectMode: true });
    this.factor = factor;
  }

  _transform(item, enc, done) {
    setTimeout(() => {
      console.log("-- Transforming item", item);
      this.push(item * this.factor);
      done();
    }, Math.random() * 100);
  }

  // _transform(item, enc, done) {
  //   this._asyncTransform(...arguments)
  //     .catch(done);
  // }

  // _asyncTransform(item, enc, done) {
  //   console.log('-- Transforming item', item);
  //   return timeout(Math.random() * 100).then(() => {
  //     this.push(item * this.factor);
  //     done();
  //   });
  //  }

  // async _asyncTransform(item, enc, done) {
  //  console.log('-- Transforming item', item);
  //  await timeout(Math.random() * 100);
  //  this.push(item * this.factor);
  //  done();
  // }
}

module.exports.AsyncMultiplierStream = AsyncMultiplierStream;

class ArrayfierStream extends stream.Writable {
  constructor() {
    super({
      objectMode: true
    });
    this.dest = [];
    this.on("finish", () => {
      this.emit("result", this.dest);
    });
  }

  _write(item, enc, cb) {
    console.log("-- Aggregating item", item);
    this.dest.push(item);
    cb();
  }
}

module.exports.ArrayfierStream = ArrayfierStream;
