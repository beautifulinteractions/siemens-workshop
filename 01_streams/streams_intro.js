const {
  SequenceStream,
  MultiplierStream,
  ArrayfierStream
} = require("./streams_lib");

const sequenceStream = new SequenceStream(0, 9);
const multiplierStream = new MultiplierStream(2);
const arrayfierStream = new ArrayfierStream();

sequenceStream
  .pipe(multiplierStream)
  .pipe(arrayfierStream)
  .on("finish", () => {
    console.log("=> Array", arrayfierStream.dest);
  });
