
const {sequencer} = require('../03_iterators/iterators_lib');
const {timeout} = require('../00_promises/promises_lib');
const {asyncMultiplier} = require('./asynciterators_lib');

const sequenceIterator = sequencer(0, 9);
const multiplyIterator = asyncMultiplier(sequenceIterator, 2);

(async () => {

	for await (const item of multiplyIterator) {
		console.log('=> Multiplied item', item);
		await timeout(Math.random() * 10);
	}

})();
