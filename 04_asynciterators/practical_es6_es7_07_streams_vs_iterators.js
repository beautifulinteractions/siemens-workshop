
const stream = require('stream');

class SequenceStream extends stream.Readable {

	constructor(start, end) {
		super({
			objectMode: true,
			highWaterMark: 1
		});
		this.start = start;
		this.curr = start;
		this.end = end;
	}

	_read(count) {
		for (let remaining = count; remaining > 0; remaining -= 1) {
			this.push(this.curr);
			this.curr += 1;
			if (this.curr > this.end) {
				this.push(null);
				return;
			}
		}
	}

}

class MultiplierStream extends stream.Transform {

	constructor(factor) {
		super({objectMode: true});
		this.factor = factor;
	}

	_transform(item, enc, done) {
		this.push(item * this.factor);
		done();
	}

}





async function* sequencer(start, end) {
	let curr = start;
	while (curr <= end) {
		yield curr;
		curr += 1;
	}
}

async function* multiplier(iterator, factor) {
	for await (const item of iterator) {
		yield item * factor;
	}
}













function waitForEvent(emitter, event) {
	return new Promise((resolve, reject) => {
		function onEvent() {
			resolve([...arguments]);
			emitter.removeListener('error', onError);
		}
		function onError(err) {
			reject(err);
			emitter.removeListener(event, onEvent);
		}
		emitter.once(event, onEvent);
		emitter.once('error', onError);
	});
}












(async () => {

	const beforeStreams = process.hrtime();

	const multiplierStream = new SequenceStream(0, 1e7 - 1)
		.pipe(new MultiplierStream(2))
		.on('data', (item) => {
			if (item < 0) {
				throw new Error('Impossible');
			}
		});

	await waitForEvent(multiplierStream, 'end');

	const deltaStreams = process.hrtime(beforeStreams);

	console.log(`Stream-based processing took ${((deltaStreams[0] * 1e9) + deltaStreams[1]) / 1e6}ms`);





	const beforeIterators = process.hrtime();

	const sequenceIterator = sequencer(0, 1e7 - 1);
	const multiplierIterator = multiplier(sequenceIterator, 2);
	
	for await (const item of multiplierIterator) {
		if (item < 0) {
			throw new Error('Impossible');
		}
	}

	const deltaIterators = process.hrtime(beforeIterators);

	console.log(`Iterator-based processing took ${((deltaIterators[0] * 1e9) + deltaIterators[1]) / 1e6}ms`);

})();
