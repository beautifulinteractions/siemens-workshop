
const {SequenceStream} = require('../01_streams/streams_lib');

const sequenceStream = new SequenceStream(0, 10);

(async () => {
  for await (const item of sequenceStream) {
    console.log('=> Read item', item);
  }
})();
