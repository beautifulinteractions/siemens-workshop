
//
// ASYNC GENERATORS
// https://github.com/tc39/proposal-async-iteration
//

const {timeout} = require('../00_promises/promises_lib');

async function* asyncMultiplier(iterator, factor) {
	for await (const item of iterator) {
		await timeout(5);
		console.log('-- Multiplying item', item, 'by', factor);
		yield item * factor;
	}
}

module.exports.asyncMultiplier = asyncMultiplier;
