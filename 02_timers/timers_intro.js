
function immediateLoop() {
  console.log('IMMEDIATE');
  setImmediate(immediateLoop);
}

function timeoutLoop() {
  console.log('TIMEOUT');
  setTimeout(timeoutLoop, 0);
}

function nextTickLoop() {
  console.log('NEXT TICK');
  process.nextTick(nextTickLoop);
}

function promiseLoop() {
  console.log('PROMISE');
  Promise.resolve().then(promiseLoop);
}

immediateLoop();
timeoutLoop();
nextTickLoop();
promiseLoop();
