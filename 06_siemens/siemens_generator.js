
const fs = require('fs');
const csv = require('csv');
const path = require('path');

const MILLIS_IN_DAY = 24 * 60 * 60 * 1000;

function generateUUID() {
  var d = new Date().getTime();
  if (typeof performance !== 'undefined' && typeof performance.now === 'function'){
      d += performance.now();
  }
  return 'xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
  });
}

function generateDeviceIds(qty) {
  return Array.apply(null, {length: qty})
    .map(generateUUID);
}

function generateDailyRecords(date, minQty, maxQty) {
  const qty = minQty + Math.round(Math.random() * (maxQty - minQty));
  const dayStartDate = new Date(date);
  const records = [];
  dayStartDate.setUTCHours(0, 0, 0, 0);
  const interval = MILLIS_IN_DAY / qty;
  for (let i = 0; i < qty; i++) {
    records.push({
      datetime: new Date(dayStartDate.valueOf() + (i * interval)).toISOString(), 
      value: Math.random()
    });
  }
  return records;
}

function generateDates(qty) {
  return Array.apply(null, {length: qty})
    .map((v, i) => new Date(Date.now() - ((qty - i + 1) * 24 * 60 * 60 * 1000)));
}

function addDevices(deviceIds, acc, date) {
  deviceIds.forEach((deviceId) => {
    acc.push({deviceId, date});
  });
  return acc;
}

function addRecords(minQty, maxQty, item) {
  return {
    ...item,
    records: generateDailyRecords(item.date, minQty, maxQty)
  };
}

function writeToDisk(item) {
  csv.stringify(item.records, {columns: ['datetime', 'value'], header: true}, (err, out) => {
    fs.writeFileSync(path.join(__dirname, 'data', `data-${item.deviceId}-${item.date.toISOString().split('T')[0]}`), out, 'utf8');
  });
}

generateDates(5)
  .reduce(addDevices.bind(null, generateDeviceIds(5)), [])
  .map(addRecords.bind(null, 500, 1000))
  .forEach(writeToDisk);
