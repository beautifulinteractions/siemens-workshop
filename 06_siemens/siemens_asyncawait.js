
const fs = require('fs');
const csv = require('csv');
const util = require('util');
const path = require('path');
const stream = require('stream');
const {waitForEvent} = require('../00_promises/promises_lib');

const [readdir] = [fs.readdir].map(util.promisify);

class JSONStringifierStream extends stream.Transform {
  constructor() {
    super({
      readableObjectMode: false,
      writableObjectMode: true
    });
  }
  _transform(obj, enc, done) {
    this.push(JSON.stringify(obj) + '\n');
    done();
  }
}

class NormalizerStream extends stream.Transform {
  constructor() {
    super({objectMode: true});
  }
  _transform(obj, enc, done) {
    this.push({...obj, value: parseFloat(obj.value)});
    done();
  }
}

async function processSingleDescriptor(descriptor) {
  const writeStream = fs.createReadStream(descriptor.path)
    .pipe(csv.parse({columns: true}))
    .pipe(new NormalizerStream())
    .pipe(new JSONStringifierStream())
    .pipe(fs.createWriteStream(descriptor.target, {flags: 'a'}));
  await waitForEvent(writeStream, 'finish');
}

async function processGroupOfDescriptors(descriptors) {
  for (const descriptor of descriptors) {
    await processSingleDescriptor(descriptor);
  }
}

async function processGroupedDescriptors(groupedDescriptors) {
  await Promise.all(groupedDescriptors.map(processGroupOfDescriptors));
}

function groupDescriptorsByDeviceId(descriptors) {
  const descriptorsById = {};
  for (const descriptor of descriptors) {
    if (!descriptorsById[descriptor.deviceId]) {
      descriptorsById[descriptor.deviceId] = [];
    }
    descriptorsById[descriptor.deviceId].push(descriptor);
  }
  return Object.values(descriptorsById);
}

/*
 * Extract useful bits of information from filenames
 */
function parseFileName(file) {
  const parts = file.split('-');
  return {
    file,
    path: path.join(__dirname, 'data', file),
    target: path.join(__dirname, 'data', 'merged-' + parts[1]),
    deviceId: parts[1],
    date: new Date(parts.slice(2).join('-'))
  };
}

(async () => {
  const fileNames = (await readdir(path.join(__dirname, 'data')))
    .filter(fileName => fileName.match(/^data-/));
  const descriptors = fileNames.map(parseFileName);
  const groupedDescriptors = groupDescriptorsByDeviceId(descriptors);
  await processGroupedDescriptors(groupedDescriptors);
})();