
function hoisting1() {
	var value = 42; 				          // Function-scoped declaration, initialization
	console.log('Value:', value);
}

function hoisting2() {
	console.log('Value:', value);     // Missing declaration
}

function hoisting3() {
									                  // Hoisted function-scoped declaration
	console.log('Value:', value);	
	var value = 42;					          // Initialization
}

// hoisting1();		// Prints "Value: 42"
// hoisting2();		// Throws error
// hoisting3();		// Prints "Value: undefined"