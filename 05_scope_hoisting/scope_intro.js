
//
// SCOPE: FUNCTION vs. BLOCK
//

function scope0a() {
	var a = 0;
	{
		let a = 1;
		console.log('"a" is:', a);
	};
	console.log('"a" is:', a);
}

function scope0b() {
	let a = 0;
	{
		var a = 1;
		console.log('"a" is:', a);
	};
	console.log('"a" is:', a);
}

function scope1() {
	for (var i = 0; i < 10; i += 1) {
		console.log('"i" is:', i);
	}
	console.log('Final value of "i":', i);
}

function scope2() {
	for (let i = 0; i < 10; i += 1) {
		console.log('"i" is:', i);
	}
	console.log('Final value of "i":', i);
}

function scope3() {
	for (let i = 0; i < 10; i += 1) {
		setTimeout(() => {
			console.log('"i" is:', i);
		}, 100);
	}
}

function scope4() {
	let i;
	for (i = 0; i < 10; i += 1) {
		setTimeout(() => {
			console.log('"i" is:', i);
		}, 100);
	}
}

// scope0a();
// scope0b();
// scope1();
// scope2();
scope3();
scope4();



