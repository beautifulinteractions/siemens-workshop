
//
// GENERATORS
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Iterators_and_Generators
//

function* sequencer(start, end) {
	let curr = start;
	while (curr <= end) {
		console.log('-- Emitting item', curr);
		yield curr;
		curr += 1;
	}
}

module.exports.sequencer = sequencer;

function* multiplier(iterator, factor) {
	for (const item of iterator) {
		console.log('-- Multiplying item', item, 'by', factor);
		yield item * factor;
	}
}

module.exports.multiplier = multiplier;
