
//
// DATA SOURCE
//

const arr = [0, 1, 2, 3, 4];

//
// ITERABLE PROTOCOL
//

const iterator = arr[Symbol.iterator]();

//
// ITERATOR PROTOCOL
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Iteration_protocols
//

console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());

//
// HOW DOES for ... of WORK?
//

for (const item of arr) {
	console.log(item);
}
