
const {sequencer, multiplier} = require('./iterators_lib');

const sequenceIterator = sequencer(0, 9);
const multiplyIterator = multiplier(sequenceIterator, 2);

for (const item of multiplyIterator) {
	console.log('=> Multiplied item', item);
}
