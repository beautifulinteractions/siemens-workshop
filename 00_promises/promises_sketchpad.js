
function cbAndPromise(cb) {
  const __cbAndPromise = ((resolve, reject) => {
    setTimeout(resolve.bind(null, 42), Math.random() * 100);
  });
  if (typeof(cb) !== 'function') {
    return new Promise(__cbAndPromise);
  }
  __cbAndPromise(cb.bind(null, null), cb);
}

cbAndPromise().then((value) => {
  console.log('Promise:', value);
});

cbAndPromise((err, value) => {
  console.log('Callback:', value);
});
