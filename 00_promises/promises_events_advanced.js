
const {EventEmitter} = require('events');
const {waitForEvent} = require('./promises_lib');

const emitter1 = new EventEmitter();
const emitter2 = new EventEmitter();

const promise = (async () => {

	await Promise.all([
		waitForEvent(emitter1, 'event1'),
		waitForEvent(emitter2, 'event2')
	]);

	console.log('After!');

})();

promise.then(() => { 
	console.log('Resolved!');
});

setTimeout(() => {
	emitter1.emit('event1');
	console.log('Emitted event1');
}, 1000);

setTimeout(() => {
	emitter2.emit('event2');
	console.log('Emitted event2');
}, 2000);

console.log('Before!');


