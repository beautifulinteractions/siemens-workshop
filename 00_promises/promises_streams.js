const { waitForEvent } = require("./promises_lib");
const { SequenceStream } = require("../01_streams/streams_lib");

const sequenceStream = new SequenceStream(0, 9);

waitForEvent(sequenceStream, "end").then(() => {
  console.log("Stream ended");
});

setTimeout(() => {
  sequenceStream.on("data", () => {});
}, Math.random() * 1000);
