function printLetter(letter) {
  setTimeout(() => {
    console.log(letter);
  }, Math.random() * 100);
}

// printLetter('a');
// printLetter('b');
// printLetter('c');
// printLetter('d');
// printLetter('e');

function printLetterCb(letter, cb) {
  setTimeout(() => {
    console.log(letter);
    cb();
  }, Math.random() * 100);
}

// printLetterCb('a', () => {
//  printLetterCb('b', () => {
//      printLetterCb('c', () => {
//          printLetterCb('d', () => {
//              printLetterCb('e', () => {
//                  // done!
//              });
//          });
//      });
//  });
// });

function printLetterCbWithError(letter, cb) {
  setTimeout(() => {
    if (Math.random() > 0.8) {
      cb(new Error("A most annoying error"));
      return;
    }
    console.log(letter);
    cb();
  }, Math.random() * 100);
}

// printLetterCbWithError('a', (errA) => {
//     if (errA) {
//         console.log('error A:', err.message);
//     }
//     printLetterCbWithError('b', (errB) => {
//         if (errB) {
//             console.log('error A:', err.message);
//         }
//         printLetterCbWithError('c', (errC) => {
//             if (errC) {
//                 console.log('error A:', err.message);
//             }
//             printLetterCbWithError('d', (errD) => {
//                 if (errD) {
//                     console.log('error A:', err.message);
//                 }
//                 printLetterCbWithError('e', (errE) => {
//                     if (errE) {
//                         console.log('error A:', err.message);
//                     }
//                 });
//             });
//         });
//     });
// });

function printLetterPromise(letter) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      console.log(letter);
      resolve();
    }, Math.random() * 100);
  });
}

// printLetterPromise('a')
//     .then(() => printLetterPromise('b'))
//     .then(() => printLetterPromise('c'))
//     .then(() => printLetterPromise('d'))
//     .then(() => printLetterPromise('e'));

function printLetterPromiseWithError(letter) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (Math.random() > 0.8) {
        console.log(letter);
        resolve();
      } else {
        reject(new Error("A most annoying error"));
      }
    }, Math.random() * 100);
  });
}

printLetterPromise("a")
  .then(() => printLetterPromiseWithError("b"))
  .then(() => printLetterPromiseWithError("c"))
  .then(() => printLetterPromiseWithError("d"))
  .then(() => printLetterPromiseWithError("e"));
