
const events = require("events");
const { waitForEvent } = require("./promises_lib");

const emitter = new events.EventEmitter();

waitForEvent(emitter, "greeting").then(() => {
  console.log("Greeting emitted.");
});

setTimeout(() => {
  emitter.emit("greeting");
});
