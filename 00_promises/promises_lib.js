
function timeout(delay) {
  return new Promise(resolve => {
    setTimeout(resolve, delay);
  });
}

module.exports.timeout = timeout;
module.exports.sleep = timeout;

function waitForEvent(emitter, event) {
  return new Promise((resolve, reject) => {
    function onEvent() {
      resolve([...arguments]);
      emitter.removeListener("error", onError);
    }
    function onError(err) {
      reject(err);
      emitter.removeListener(event, onEvent);
    }
    emitter.once(event, onEvent);
    emitter.once("error", onError);
  });
}

module.exports.waitForEvent = waitForEvent;
